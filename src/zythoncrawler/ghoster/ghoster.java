package zythoncrawler.ghoster;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import com.jaunt.Element;
import com.jaunt.Elements;
import com.jaunt.NotFound;
import com.jaunt.ResponseException;
import com.jaunt.UserAgent;



public class ghoster {
	
	private String searchword;
	private PrintWriter fileprinter;
	
	
	public ghoster(String searchword){
		
		this.searchword = searchword;
	}
	
	//getter and setter for ghoster
	public String getSearchword(){
		return this.searchword;
	}
	public void setSearchword(String searchword){
		this.searchword = searchword;
	}
	
	public void ebayKAscraping(String searchquerz, File filename) throws NotFound, IOException{
	
		try {
			long start = System.currentTimeMillis();
			
			
			fileprinter = new PrintWriter(filename);
			
			
			UserAgent agent = new UserAgent();
			agent.visit("http://www.ebay-kleinanzeigen.de/s-73760/"+searchquerz+"/k0l8437r50");
			
			fileprinter.println(agent.doc.getUrl());
			
			Elements webPageElements = agent.doc.findEach("<li class=\"ad-listitem lazyload-item   ");
			System.out.println("Anzahl der Suchergebnisse "+webPageElements.size());
			for(Element e : webPageElements){
				// syso for intterhtml just for monitoring reasons, (if someting goes wrong)
				System.out.println(e.innerHTML());
				fileprinter.println("\n");
				/*name */
				fileprinter.println(e.findFirst("<h2 class=\"text-module-begin\">").findFirst("<a>").innerHTML());
				/*price*/
				fileprinter.println(e.findFirst("<section class=\"aditem-details\">").findFirst("<strong>").innerHTML());
				/*time and date*/
				fileprinter.println(e.findFirst("<section class=\"aditem-addon\">").innerHTML().replaceAll(" ", ""));
				fileprinter.println("\n");
				fileprinter.println("----------------------");
			}
			
			
			long performanceTime = System.currentTimeMillis()-start;
			System.out.println("Total Scantime in Miliseconds "+performanceTime);
		} catch (ResponseException e) {
			e.printStackTrace();
		}finally {
			System.out.println("end of scraping");
			fileprinter.close();
		}
		
	}
	
	//main method for testing ... 
	public static void main(String[] args) throws IOException {
		ghoster ghost = new ghoster("canon");
		try {
			String filepath ="C:\test.txt";
			File file = new File(filepath);
			file.getParentFile().mkdirs();
			ghost.ebayKAscraping(ghost.getSearchword(),file);
		} catch (NotFound e) {
			
			e.printStackTrace();
		}
	}
}
