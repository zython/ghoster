package zythoncrawler.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.UIManager;

import zythoncrawler.ghoster.ghoster;

public class mainGUI {
	private JFrame frmZythonWebscraper;
	private JTextField txtInsertUrl;
	private JTextField txtInsertSelector;
	private JTextField txtOutputFile;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//testing beta branch
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					mainGUI window = new mainGUI();
					window.frmZythonWebscraper.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public mainGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
		Date date = new Date();
		String timestamp = dateFormat.format(date).toString();

		frmZythonWebscraper = new JFrame();
		frmZythonWebscraper.setTitle("Zython Ghoster v0.1");
		frmZythonWebscraper.setBounds(100, 100, 474, 397);
		frmZythonWebscraper.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel panel = new JPanel();
		frmZythonWebscraper.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		JButton btnGo = new JButton("Go !");
		btnGo.setBounds(365, 336, 91, 23);
		panel.add(btnGo);

		txtInsertUrl = new JTextField();
		txtInsertUrl.setText("giant");
		txtInsertUrl.setBounds(10, 11, 438, 23);
		panel.add(txtInsertUrl);
		txtInsertUrl.setColumns(10);

		JRadioButton rdbtnEnterOwnSelector = new JRadioButton("Enter own selector");
		rdbtnEnterOwnSelector.setEnabled(false);
		rdbtnEnterOwnSelector.setBounds(10, 41, 150, 23);
		panel.add(rdbtnEnterOwnSelector);

		JRadioButton rdbtnChooseFromPredefined = new JRadioButton("Choose from predefined selectors");
		rdbtnChooseFromPredefined.setEnabled(false);
		rdbtnChooseFromPredefined.setBounds(10, 103, 187, 23);
		panel.add(rdbtnChooseFromPredefined);

		txtInsertSelector = new JTextField();
		txtInsertSelector.setEnabled(false);
		txtInsertSelector.setText("insert selector");
		txtInsertSelector.setColumns(10);
		txtInsertSelector.setBounds(10, 71, 438, 23);
		panel.add(txtInsertSelector);

		JSeparator separator = new JSeparator();
		separator.setBounds(10, 133, 446, 2);
		panel.add(separator);

		txtOutputFile = new JTextField();
		txtOutputFile.setText("C:/ghoster/" + timestamp + ".txt");

		txtOutputFile.setBounds(10, 146, 255, 20);
		panel.add(txtOutputFile);
		txtOutputFile.setColumns(10);

		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(10, 177, 446, 1);
		panel.add(separator_1);

		
		JEditorPane editorPane = new JEditorPane();
		editorPane.setBounds(10, 189, 446, 136);
		panel.add(editorPane);
		
		
		
		
		
		btnGo.addActionListener(a -> {
			File outputFile = new File(txtOutputFile.getText());	
			outputFile.getParentFile().mkdirs();
			String validSearch = txtInsertUrl.getText().replaceAll(" ", "-");
			System.out.println(validSearch);
			try {
				
				ghoster ghost = new ghoster(validSearch);
				ghost.ebayKAscraping(ghost.getSearchword(), outputFile);

			} catch (Exception e) {
				System.out.println("something happened");
				e.printStackTrace();
			} finally {
				System.out.println("Scraping done in Main GUI");
				try {
					editorPane.setPage(outputFile.toURI().toURL());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});
	}
}
