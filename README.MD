This is a WIP webscraper for my personal use.

I intend to automate my most timeconsuming browsing habits with this scraper.

As of 7th-FEB-2016 it can scrape the top search results from a craigslist-like website and store them in a file. 

This scraper also features a GUI which is going to be build into all of my desires.

**This are the websites that are already "supported":**
- ebay-kleinanzeigen

**This are the websites that are planned to be implemented:**
- reddit (with custom subreddits)
- maybe include other platforms to scrape simultaniously with ebay kleinanzeigen
- (...)